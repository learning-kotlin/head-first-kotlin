import kotlin.random.Random.Default.nextInt

fun main() {
    val wordArray1 = arrayOf("24/7", "multi-tier", "B-to-B", "dynamic", "pervasive")
    val wordArray2 = arrayOf("empowered", "leveraged", "aligned", "targeted")
    val wordArray3 = arrayOf("process", "paradigm", "solution", "portal", "vision")

    val arraySize1 = wordArray1.size
    val arraySize2 = wordArray2.size
    val arraySize3 = wordArray3.size

    // Kotlin version < 1.3
    val rand1 = (Math.random() * arraySize1).toInt()
    val rand2 = (Math.random() * arraySize2).toInt()
    val rand3 = (Math.random() * arraySize3).toInt()

    // Kotlin version >= 1.3
    val rand4 = (nextInt(arraySize1))
    val rand5 = (nextInt(arraySize2))
    val rand6 = (nextInt(arraySize3))

    val phrase = "${wordArray1[rand1]} ${wordArray2[rand2]} ${wordArray3[rand3]}"
    println(phrase)

    println("${wordArray1[rand4]} ${wordArray2[rand5]} ${wordArray3[rand6]}")
}
